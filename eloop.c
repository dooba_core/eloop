/* Dooba SDK
 * Generic Main Loop Wrapper
 */

// Internal Includes
#include "eloop.h"

// Re-init
uint8_t eloop_reinit;

// Root Hook
struct eloop_hook *eloop_root;

/* Run a main loop - this requires two methods to be implemented:
 * void init() - called once during startup
 * void loop() - called repeatedly until power off
 */
#ifdef	ELOOP_MAIN
void main()
{
	// Run Loop
	eloop();
}
#endif

// Run Loop
void eloop()
{
	// Perform Initialization
	eloop_run_init();

	// Loop
	for(;;)								{ eloop_update(); if(eloop_reinit) { eloop_run_init(); } }
}

// Hook Loop (Attach a method to the main loop)
void eloop_hook(struct eloop_hook *h, eloop_hook_t meth, void *user)
{
	// Insert Hook
	h->meth = meth;
	h->user = user;

	// Register into Chain
	h->next = eloop_root;
	h->prev = 0;
	if(eloop_root)						{ eloop_root->prev = h; }
	eloop_root = h;
}

// Un-Hook Loop (Detach method from main loop)
void eloop_unhook(struct eloop_hook *h)
{
	// Remove from Chain
	if(h->next)							{ h->next->prev = h->prev; }
	if(h->prev)							{ h->prev->next = h->next; }
	if(eloop_root == h)					{ eloop_root = h->next; }
}

// Request Re-init
void eloop_request_reinit()
{
	// Request Re-init
	eloop_reinit = 1;
}

// Run Loop Init
void eloop_run_init()
{
	// Clear Re-init
	eloop_reinit = 0;

	// Setup Root Hook
	eloop_root = 0;

	// Perform Initialization
	init();
}

// Run Hooks
void eloop_run_hooks()
{
	struct eloop_hook *h;

	// Run through hooks
	h = eloop_root;
	while(h)							{ h->meth(h->user); h = h->next; }
}
