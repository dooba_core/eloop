/* Dooba SDK
 * Generic Main Loop Wrapper
 */

#ifndef	__ELOOP_H
#define	__ELOOP_H

// External Includes
#include <stdint.h>

// Provide Main by default
#ifndef	ELOOP_NO_MAIN
#ifndef	ELOOP_MAIN
#define	ELOOP_MAIN
#endif
#endif

// Update
#define	eloop_update()								eloop_run_hooks(); loop()

// Hook Type
typedef	void (*eloop_hook_t)(void *user);

// Hook Structure
struct eloop_hook
{
	// Method
	eloop_hook_t meth;

	// User Data
	void *user;

	// List
	struct eloop_hook *next;
	struct eloop_hook *prev;
};

// External Methods
extern void init();
extern void loop();

// Run Loop
extern void eloop();

// Hook Loop (Attach a method to the main loop)
extern void eloop_hook(struct eloop_hook *h, eloop_hook_t meth, void *user);

// Un-Hook Loop (Detach method from main loop)
extern void eloop_unhook(struct eloop_hook *h);

// Request Re-init
extern void eloop_request_reinit();

// Run Loop Init
extern void eloop_run_init();

// Run Hooks
extern void eloop_run_hooks();

#endif
